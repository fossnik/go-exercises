package collatzconjecture

import "errors"

func CollatzConjecture(n int) (int, error) {
	// if n < 1, return error
	if n < 1 {
		return 0, errors.New("n cannot be less than 1")
	}

	// count how many iterations it takes to get to 1
	var count int

	// iterate while n doesn't equal 1
	for ; n != 1; count++ {
		if n%2 == 0 { // if even, divide n by 2
			n = n / 2
		} else { // if odd, multiply by 3 and add 1
			n = (n * 3) + 1
		}
	}

	return count, nil
}
