package hamming

import "fmt"

func Distance(a, b string) (int, error) {

	// return error if the strings are different lengths
	if lengthOfA, lengthOfB := len(a), len(b); lengthOfA != lengthOfB {
		return 0, fmt.Errorf("Error! Strings are different lengths:\n"+
			"a length: %v\n"+
			"b length: %v", lengthOfA, lengthOfB)
	}

	// count of differences between the two strings
	distance := 0

	// loop through the range and count all the differences
	for i, _ := range a {
		if a[i] != b[i] {
			distance++
		}
	}

	// return the distance, and a nil error (no error)
	return distance, nil
}
