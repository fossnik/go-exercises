package isogram

import "strings"

func IsIsogram(word string) bool {
	// create a frequency map for letters
	occurrence := make(map[rune]int)

	// get a lower-case only version
	lower := strings.ToLower(word)

	// keep track of each letter
	for _, r := range lower {
		occurrence[r]++
	}

	// look through the map to find any letter with multiple occurrence
	for _, r := range "abcdefghijklmnopqrstuvwxyz" {
		if occurrence[r] > 1 {
			return false
		}
	}

	// no multiple occurrence found so return true
	return true
}
