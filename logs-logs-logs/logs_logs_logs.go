package logs

import (
	"fmt"
	"unicode/utf8"
)

// Application identifies the application emitting the given log.
func Application(log string) string {
	// loop through the runes
	for _, char := range log {
		if char == '❗' {
			return "recommendation"
		}
		if char == '🔍' {
			return "search"
		}
		if char == '☀' {
			return "weather"
		}
	}

	// did not find a rune
	return "default"
}

// Replace replaces all occurrences of old with new, returning the modified log
// to the caller.
func Replace(log string, oldRune, newRune rune) string {
	// create a new string
	newString := ""

	// loop through and check for runes matching oldRune
	for _, char := range log {
		if char == oldRune {
			// when an oldRule is found, replace with newRune
			newString += fmt.Sprintf("%c", newRune)
		} else {
			// otherwise add to the string as-is
			newString += fmt.Sprintf("%c", char)
		}
	}

	// return the new string
	return newString
}

// WithinLimit determines whether or not the number of characters in log is
// within the limit.
func WithinLimit(log string, limit int) bool {
	return utf8.RuneCountInString(log) <= limit
}
