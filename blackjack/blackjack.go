package blackjack

// ParseCard returns the integer value of a card following blackjack ruleset.
func ParseCard(card string) int {
	switch card {
	case "ace":
		return 11
	case "two":
		return 2
	case "three":
		return 3
	case "four":
		return 4
	case "five":
		return 5
	case "six":
		return 6
	case "seven":
		return 7
	case "eight":
		return 8
	case "nine":
		return 9
	case "ten", "jack", "queen", "king":
		return 10
	default:
		return 0
	}
}

// FirstTurn returns the decision for the first turn, given two cards of the
// player and one card of the dealer.
func FirstTurn(card1, card2, dealerCard string) string {
	// if you have a pair of aces you must always split them
	if card1 == "ace" && card2 == "ace" {
		return "P"
	}

	// get numerical value of cards
	sum := ParseCard(card1) + ParseCard(card2)

	// get value of dealer card
	dealerCardValue := ParseCard(dealerCard)

	// If you have a Blackjack (two cards that sum up to a value of 21),
	if sum == 21 {
		// and the dealer does not have an ace, a figure or a ten then you automatically win.
		if dealerCardValue < 10 {
			return "W"
		} else {
			//If the dealer does have any of those cards then you'll have to stand and wait for the reveal of the other card.
			return "S"
		}
	}

	// If your cards sum up to 11 or lower you should always hit.
	if sum <= 11 {
		return "H"
	}

	// If your cards sum up to a value within the range [17, 20] you should always stand.
	if sum >= 17 && sum <= 20 {
		return "S"
	}

	//	If your cards sum up to a value within the range [12, 16]
	if sum >= 17 && sum <= 20 {
		// you should always stand unless the dealer has a 7 or higher,
		if dealerCardValue < 7 {
			return "S"
		} else {
			// in which case you should always hit.
			return "H"
		}
	}

	if dealerCardValue == 6 {
		return "S"
	} else {
		return "H"
	}

}
