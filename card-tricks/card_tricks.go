package cards

// FavoriteCards returns a slice with the cards 2, 6 and 9 in that order.
func FavoriteCards() []int {
	// initialize a slice
	favoriteCards := []int{2, 6, 9}

	// return the created slice
	return favoriteCards
}

// GetItem retrieves an item from a slice at given position.
// If the index is out of range, we want it to return -1.
func GetItem(slice []int, index int) int {
	// test if the index is OOB
	if len(slice) > index && index >= 0 {
		// get the item at the index
		item := slice[index]

		// return the item
		return item

	} else {
		return -1
	}

}

// SetItem writes an item to a slice at given position overwriting an existing value.
// If the index is out of range the value needs to be appended.
func SetItem(slice []int, index, value int) []int {
	// if the index is -1 just append it
	if index < 0 {
		slice = append(slice, value)

		return slice
	}

	// test if index is out of bounds
	if len(slice) <= index {
		// append if index is OOB
		slice = append(slice, value)

	} else {
		// set the item at that index to the given value
		slice[index] = value
	}

	// return the new slice
	return slice
}

// PrependItems adds an arbitrary number of values at the front of a slice.
func PrependItems(slice []int, values ...int) []int {
	// append values to create a new slice
	slice = append(values, slice...)

	// return the new slice
	return slice
}

// RemoveItem removes an item from a slice by modifying the existing slice.
func RemoveItem(slice []int, index int) []int {
	// text if index is OOB
	if index < 0 || index > len(slice) {
		return slice
	}

	// remove an item at the index
	slice = append(slice[:index], slice[index+1:]...)

	// return the new slice
	return slice
}
