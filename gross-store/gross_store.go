package gross

// Units stores the Gross Store unit measurements.
func Units() map[string]int {
	// create the map literal
	grossUnits := make(map[string]int)

	// add the units
	grossUnits["quarter_of_a_dozen"] = 3
	grossUnits["half_of_a_dozen"] = 6
	grossUnits["dozen"] = 12
	grossUnits["small_gross"] = 120
	grossUnits["gross"] = 144
	grossUnits["great_gross"] = 1728

	return grossUnits
}

// NewBill creates a new bill.
func NewBill() map[string]int {
	// create the map literal for a customer bill
	return make(map[string]int)
}

// AddItem adds an item to customer bill.
func AddItem(bill, units map[string]int, item, unit string) bool {
	var amount, exists = units[unit]

	// return false if the unit is not in the units map
	if !exists {
		return false
	} else {
		// If the item is already present in the bill, increase its quantity by the amount that belongs to the provided unit.
		var _, isAlreadyOnBill = bill[item]

		if isAlreadyOnBill {
			bill[item] += amount
		} else {
			// add the item to the customer bill
			bill[item] = amount
		}

		return true
	}
}

// RemoveItem removes an item from customer bill.
func RemoveItem(bill, units map[string]int, item, unit string) bool {
	// Return false if the given item is not in the bill
	var amountOfItemOnBill, itemIsInBill = bill[item]
	if !itemIsInBill {
		return false
	}

	// Return false if the given unit is not in the units map.
	var amountToRemoveFromBill, unitIsInUnitsMap = units[unit]
	if !unitIsInUnitsMap {
		return false
	}

	// Return false if the new quantity would be less than 0.
	if amountToRemoveFromBill > amountOfItemOnBill {
		return false
	}

	// If the new quantity is 0, completely remove the item from the bill then return true.
	if amountOfItemOnBill == amountToRemoveFromBill {
		delete(bill, item)
		return true
	}

	// Otherwise, reduce the quantity of the item and return true.
	bill[item] -= amountToRemoveFromBill
	return true
}

// GetItem returns the quantity of an item that the customer has in his/her bill.
func GetItem(bill map[string]int, item string) (int, bool) {
	var quantity, exists = bill[item]

	if exists {
		return quantity, true
	} else {
		return 0, false
	}
}
