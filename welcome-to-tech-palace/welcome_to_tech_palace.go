package techpalace

import "strings"

// WelcomeMessage returns a welcome message for the customer.
func WelcomeMessage(customer string) string {
	// get a customer's name as all upper case
	customerUPPER := strings.ToUpper(customer)

	// returns the prefix welcome message, and the username
	return "Welcome to the Tech Palace, " + customerUPPER
}

// AddBorder adds a border to a welcome message.
func AddBorder(welcomeMsg string, numStarsPerLine int) string {
	var outputString = ""

	// add the top boarder
	outputString += strings.Repeat("*", numStarsPerLine)

	// add newline
	outputString += "\n"

	// add message
	outputString += welcomeMsg

	// add newline
	outputString += "\n"

	// add bottom boarder
	outputString += strings.Repeat("*", numStarsPerLine)

	return outputString
}

// CleanupMessage cleans up an old marketing message.
func CleanupMessage(oldMsg string) string {
	// replace all stars with nothing
	var newString = strings.ReplaceAll(oldMsg, "*", "")

	// trim all whitespace
	var newerString = strings.TrimSpace(newString)

	return newerString
}
