package chessboard

// Declare a type named File which stores if a square is occupied by a piece - this will be a slice of bools
type File []bool

// Declare a type named Chessboard which contains a map of eight Files, accessed with keys from "A" to "H"
type Chessboard map[string]File

// CountInFile returns how many squares are occupied in the chessboard,
// within the given file.
func CountInFile(cb Chessboard, file string) int {
	// value to count the number of occupied squares
	var count = 0

	// loop through each square in the specified file
	for _, square := range cb[file] {
		// count each occupied square
		if square {
			count++
		}
	}

	return count
}

// CountInRank returns how many squares are occupied in the chessboard,
// within the given rank.
func CountInRank(cb Chessboard, rank int) int {
	// return 0 if not a valid rank
	if rank > 8 || rank < 1 {
		return 0
	}

	count := 0

	// loop through each row
	for _, row := range cb {
		if row[rank-1] == true {
			count++
		}
	}

	return count
}

// CountAll should count how many squares are present in the chessboard.
func CountAll(cb Chessboard) int {
	// establish a counter variable
	count := 0

	// loop through files
	for _, file := range cb {
		// loop through squares within file
		for range file {
			count++
		}
	}

	// return the count
	return count
}

// CountOccupied returns how many squares are occupied in the chessboard.
func CountOccupied(cb Chessboard) int {
	// establish a counter variable
	count := 0

	// loop through files
	for _, file := range cb {
		// loop through squares within file
		for _, square := range file {
			// test if the square is occupied
			if square {
				count++

			}
		}
	}

	// return the count
	return count
}
