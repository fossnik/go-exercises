// Package weather contains functionality related to weather reporting
package weather

// CurrentCondition describes the current condition of the weather.
var CurrentCondition string

// CurrentLocation describes the current location.
var CurrentLocation string

// Forecast is a function that generates a description of the weather at a given location.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
