package raindrops

import "strconv"

func Convert(number int) string {
	rem := ""

	if number%3 == 0 {
		rem += "Pling"
	}
	if number%5 == 0 {
		rem += "Plang"
	}
	if number%7 == 0 {
		rem += "Plong"
	}

	if rem == "" {
		return strconv.Itoa(number)
	}

	return rem
}
