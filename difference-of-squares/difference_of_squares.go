package diffsquares

func SquareOfSum(n int) int {
	// store the sum
	sum := 0

	// sum the natural numbers up to n
	for i := 0; i <= n; i++ {
		sum += i
	}

	// return the square of the sum
	return sum * sum
}

func SumOfSquares(n int) int {
	// store the sum
	sum := 0

	// sum the (squares of) natural numbers up to n
	for i := 0; i <= n; i++ {
		sum += i * i
	}

	// return the sum
	return sum
}

func Difference(n int) int {
	diff := func(a, b int) int {
		if a < b {
			return b - a
		}
		return a - b
	}

	// return the difference between SumOfSquares and SquareOfSums
	return diff(SumOfSquares(n), SquareOfSum(n))
}
