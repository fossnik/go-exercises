package pangram

import "strings"

func IsPangram(input string) bool {
	// convert the string to all lower-case
	input = strings.ToLower(input)

	// loop through the whole alphabet, seeking instance of each letter
	for _, c := range "abcdefghijklmnopqrstuvwxyz" {
		// return false if the letter isn't found
		if !strings.Contains(input, string(c)) {
			return false
		}
	}

	return true
}
