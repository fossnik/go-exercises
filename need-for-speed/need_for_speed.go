package speed

// Car defines a car struct
type Car struct {
	battery      int
	batteryDrain int
	speed        int
	distance     int
}

// NewCar creates a new remote controlled car with full battery and given specifications.
func NewCar(speed, batteryDrain int) Car {
	return Car{
		battery:      100,
		batteryDrain: batteryDrain,
		speed:        speed,
		distance:     0,
	}
}

// Track defines a track struct
type Track struct {
	distance int
}

// NewTrack creates a new track
func NewTrack(distance int) Track {
	return Track{
		distance: distance,
	}
}

// Drive drives the car one time. If there is not enough battery to drive one more time,
// the car will not move.
func Drive(car Car) Car {
	// if there's enough battery to run the car, update the car
	if car.battery >= car.batteryDrain {
		// update the number of meters driven based on car's speed
		car.distance += car.speed

		// reduce the battery according to the battery drainage
		car.battery -= car.batteryDrain
	}

	// return the car
	return car
}

// CanFinish checks if a car is able to finish a certain track.
func CanFinish(car Car, track Track) bool {
	// drive the car until it's distance exceeds the track distance
	for car.distance < track.distance {
		// if the car doesn't have enough battery, it can't finish
		if car.battery <= car.batteryDrain {
			// it's a fail
			return false
		} else {
			// drive the car
			car = Drive(car)
		}
	}

	return true
}
