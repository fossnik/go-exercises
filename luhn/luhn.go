package luhn

import (
	"strconv"
	"strings"
)

func Valid(id string) bool {
	// remove all the whitespace
	id = strings.ReplaceAll(id, " ", "")

	// reverse the order of the string
	result := ""
	for _, v := range id {
		result = string(v) + result
	}
	id = result

	// sum value for all digits
	sum := 0

	// loop through each char in the string
	for i, char := range id {
		// convert the character to an integer
		digit, err := strconv.Atoi(string(char))
		if err != nil {
			// return false because a non-numerical value was presented
			return false
		}

		// test if the index is an even number
		if i%2 == 0 {
			// add the value to the sum
			sum += digit
		} else {
			// for odd-indexed values, double them
			doubleNum := digit * 2

			// test if the new amount is more than 9
			if doubleNum > 9 {
				// if more than 9, reduce value by 9
				doubleNum -= 9
			}

			// add the value to the sum
			sum += doubleNum
		}
	}

	// if the sum is divisible by 10 and input length > 1, return true
	return sum%10 == 0 && len(id) > 1
}
