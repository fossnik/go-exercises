package rotationalcipher

import (
	"strings"
	"unicode"
)

/*
RotationalCipher applies a rotational cipher to encrypt the given plaintext
using the specified shift key
*/
func RotationalCipher(plain string, shiftKey int) (newString string) {

	// rotateChar applies a specified shift to a given character and returns the rotated character
	rotateChar := func(plainLetter rune) (rotatedLetter rune) {
		// check if the character is a non-letter
		if !unicode.IsLetter(plainLetter) {
			// return the letter as-is
			return plainLetter
		}

		// initialize the alphabet sequence based on the case of the letter
		alphaSequence := "abcdefghijklmnopqrstuvwxyz"
		if unicode.IsUpper(plainLetter) {
			alphaSequence = strings.ToUpper(alphaSequence)
		}

		// get the index at which the given letter occurs in the alphabet sequence
		indexOfPlainLetter := strings.Index(alphaSequence, string(plainLetter))

		// apply the shift key to the given letter, bounded by the length of the alphabet
		indexOfRotatedLetter := (indexOfPlainLetter + shiftKey) % len(alphaSequence)

		// get the rotated letter by using the index provided by our shift calculation
		rotatedLetter = rune(alphaSequence[indexOfRotatedLetter])

		// return the rotated letter
		return
	}

	// get the encrypted string by mapping plain text through the rotateChar function
	newString = strings.Map(rotateChar, plain)

	// return the encrypted text
	return
}
