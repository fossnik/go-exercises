package lasagna

// accepts a slice of layers as a []string and the average preparation time per layer in minutes as an int.
// return the estimate for the total preparation time based on the number of layers as an int
func PreparationTime(layers []string, minutes int) int {
	// If the average preparation time is passed as 0 (the default initial value for an int), then the default value of 2 should be used.
	if minutes == 0 {
		minutes = 2
	}

	// calculate needed time by number of layers * minutes
	prepEstimate := len(layers) * minutes

	return prepEstimate
}

// Returns the amount of necessary ingredients
func Quantities(layers []string) (noodles int, sauce float64) {
	for i := 0; i < len(layers); i++ {
		// For each noodle layer in your lasagna, you will need 50 grams of noodles.
		if layers[i] == "noodles" {
			noodles += 50
		} else {
			// For each sauce layer in your lasagna, you will need 0.2 liters of sauce.
			if layers[i] == "sauce" {
				sauce += .2
			}
		}
	}

	// returns the named values
	return
}

// accepts two slices of ingredients of type []string as parameters. The first parameter is the list your friend sent you, the second is the ingredient list of your own recipe. The last element in your ingredient list is always "?". The function should replace it with the last item from your friends list.
func AddSecretIngredient(friendsList []string, myList []string) {
	// replace last item in my list with last item in friend's list
	myList[len(myList)-1] = friendsList[len(friendsList)-1]
}

// Implement a function ScaleRecipe that takes two parameters.
// - A slice of float64 amounts needed for 2 portions.
// - The number of portions you want to cook.
// The function should return a slice of float64 of the amounts needed for the desired number of portions.
func ScaleRecipe(amountNeeded []float64, numPortions int) []float64 {
	// create a new list
	amountNeededNew := make([]float64, len(amountNeeded))

	// loop through each slice
	for i, quantity := range amountNeeded {
		// figure out how much is need for the new portion size
		newAmount := (quantity / 2.0) * float64(numPortions)

		// add the new portion size to our list for return
		amountNeededNew[i] = newAmount
	}

	return amountNeededNew
}
