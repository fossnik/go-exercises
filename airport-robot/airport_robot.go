package airportrobot

import "fmt"

// interface Greeter specifies functions related to greeting guests
type Greeter interface {
	// returns the name of the language (a string) that the robot is supposed to greet the visitor in.
	LanguageName() string

	// accepts a visitor's name (a string) and returns a string with the greeting message in a specific language.
	Greet(name string) string
}

// accepts the name of the visitor and anything that implements the Greeter interface as arguments and returns the desired greeting string.
func SayHello(name string, greeter Greeter) string {
	return fmt.Sprintf("I can speak %s: %s", greeter.LanguageName(), greeter.Greet(name))
}

type Italian struct{}

func (i Italian) LanguageName() string {
	return "Italian"
}

func (i Italian) Greet(name string) string {
	return fmt.Sprintf("Ciao %s!", name)
}

type Portuguese struct{}

func (i Portuguese) LanguageName() string {
	return "Portuguese"
}

func (i Portuguese) Greet(name string) string {
	return fmt.Sprintf("Olá %s!", name)
}
