package cars

// CalculateWorkingCarsPerHour calculates how many working cars are
// produced by the assembly line every hour.
func CalculateWorkingCarsPerHour(productionRate int, successRate float64) float64 {
	// convert production rate to 64 bit float
	var productionRate64 = float64(productionRate)

	// calculate working cars produced per hour
	var workingCarsPerHour = productionRate64 * successRate / 100.0

	// return workingCarsPerHour
	return workingCarsPerHour
}

// CalculateWorkingCarsPerMinute calculates how many working cars are
// produced by the assembly line every minute.
func CalculateWorkingCarsPerMinute(productionRate int, successRate float64) int {
	// convert production rate to 64 bit float
	var productionRate64 = float64(productionRate)

	// calculate how many cars are produced every minute
	var workingCarsPerMinute = productionRate64 * successRate / 6000.0

	// convert to an int
	var workingCarsPerMinuteInt = int(workingCarsPerMinute)

	// return workingCarsPerminuteInt
	return workingCarsPerMinuteInt
}

// CalculateCost works out the cost of producing the given number of cars.
func CalculateCost(carsCount int) uint {
	// get how many cars need to be produced outside of 10-unit groups
	var individualCars = carsCount % 10

	// get how many groups of 10 can be produced
	var groupsOfTen = (carsCount - individualCars) / 10

	// calculate the total cost of producing the cars
	var totalCost = individualCars*10000 + groupsOfTen*95000

	// get totalCost as a unsigned int
	var totalCostUint = uint(totalCost)

	// return totalCostUint
	return totalCostUint
}
