package elon

import "fmt"

func (c *Car) Drive() {
	// update car only if it can make the distance
	if c.battery >= c.batteryDrain {
		// update the number of meters driven based on speed
		c.distance += c.speed

		// reduce the battery according to battery drainage
		c.battery -= c.batteryDrain
	}
}

func (c Car) DisplayDistance() string {
	return fmt.Sprint("Driven ", c.distance, " meters")
}

func (c Car) DisplayBattery() string {
	return fmt.Sprint("Battery at ", c.battery, "%")
}

func (c Car) CanFinish(trackDistance int) bool {
	return c.battery/c.batteryDrain*c.speed >= trackDistance
}
