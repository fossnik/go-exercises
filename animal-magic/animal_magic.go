package chance

import "math/rand"

// RollADie returns a random int d with 1 <= d <= 20.
func RollADie() int {
	return rand.Intn(19) + 1
}

// GenerateWandEnergy returns a random float64 f with 0.0 <= f < 12.0.
func GenerateWandEnergy() float64 {
	return rand.Float64() * 12
}

// ShuffleAnimals returns a slice with all eight animal strings in random order.
func ShuffleAnimals() []string {
	// all the animals
	lot := []string{"ant", "beaver", "cat", "dog", "elephant", "fox", "giraffe", "hedgehog"}

	// slice for output data
	outputSlice := []string{}

	// loop through it while there's 1 or more items left
	for len(lot) > 0 {
		// pick a random index (animal)
		index := rand.Intn(len(lot))

		// add that index to the output slice
		outputSlice = append(outputSlice, lot[index])

		// remove that index from the original slice
		lot = append(lot[:index], lot[index+1:]...)
	}

	// return the new slice array
	return outputSlice
}
