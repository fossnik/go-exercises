package leap

// IsLeapYear determines if a given year is a leap year
func IsLeapYear(year int) bool {
	if divisibleBy(year, 400) {
		return true
	}
	if divisibleBy(year, 100) {
		return false
	}

	return divisibleBy(year, 4)
}

// returns true if the topNumber divides evenly into the bottom number
func divisibleBy(topNum int, bottomNum int) bool {
	return topNum%bottomNum == 0
}
