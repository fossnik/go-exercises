package purchase

// NeedsLicense determines whether a license is needed to drive a type of vehicle. Only "car" and "truck" require a license.
func NeedsLicense(kind string) bool {
	// needLicense assumes that you do not need a license
	needLicense := false

	// if the kind is a car or truck, set needLicense to true
	if kind == "car" || kind == "truck" {
		needLicense = true
	}

	return needLicense
}

// ChooseVehicle recommends a vehicle for selection. It always recommends the vehicle that comes first in lexicographical order.
func ChooseVehicle(option1, option2 string) string {
	// pick car based on lexigraphical order
	if option1 < option2 {
		return option1 + " is clearly the better choice."
	} else {
		return option2 + " is clearly the better choice."
	}
}

// CalculateResellPrice calculates how much a vehicle can resell for at a certain age.
func CalculateResellPrice(originalPrice, age float64) float64 {
	// costFactor is a ratio for depreciation of the vehicle over time
	costFactor := 0.0

	// set the costFactor based on age of the vehicle
	if age < 3 {
		costFactor = 0.8
	} else if age >= 10 {
		costFactor = 0.5
	} else {
		costFactor = 0.7
	}

	// return a cost based on the original price with depreciation factored in
	return originalPrice * costFactor
}
