package parsinglogfiles

import (
	"fmt"
	"regexp"
)

func IsValidLine(text string) bool {
	// this is my registry expression, enclosed in backticks
	// \[(TRC|DBG|INF|WRN|ERR|FTL)\] matches any of the specified strings enclosed in square brackets:
	//(TRC|DBG|INF|WRN|ERR|FTL) is a capturing group that matches any of the strings inside the parentheses separated by the | (OR) operator.
	//\] matches the closing square bracket.
	re := regexp.MustCompile(`^\[(TRC|DBG|INF|WRN|ERR|FTL)\]`)

	// it will return true if it can match the string with my regular expression
	return re.MatchString(text)
}

func SplitLogLine(text string) []string {
	// This regex pattern will match -~*= between < and >
	separatorPattern := `<[-~*=]*>`

	// Compile the regular expression
	re := regexp.MustCompile(separatorPattern)

	// split into as many segments as possible based on the regex
	return re.Split(text, -1)
}

func CountQuotedPasswords(lines []string) int {
	// Regular expression pattern to match "password" surrounded by double quotes
	// (?i) is added at the beginning of the regular expression pattern to make it case insensitive.
	// passwordPattern now matches "password" surrounded by double quotes, regardless of the case,
	// and allowing for additional content between the quotation marks before and after "password".
	passwordPattern := `(?i)"[^"]*password[^"]*"`

	// Compiled regular expression
	re := regexp.MustCompile(passwordPattern)

	// Count of how many times "password" shows up
	count := 0

	// Count how many times "password" appears
	for _, line := range lines {
		if re.MatchString(line) {
			count++
		}
	}

	// return the count
	return count
}

func RemoveEndOfLineText(text string) string {
	// matches end-of-line plus numbers pattern
	endOfLinePattern := `end-of-line\d+`

	// regular expression compiled
	re := regexp.MustCompile(endOfLinePattern)

	// return it clean (replace with null string)
	return re.ReplaceAllString(text, "")
}

func TagWithUserName(lines []string) []string {
	// create an array of strings to return
	taggedLines := []string{}

	// regex pattern to match User + space + a username of words
	userMatchString := `User\s+(\w+)`

	// compile regular expression
	re := regexp.MustCompile(userMatchString)

	// loop through each line
	for _, line := range lines {
		// if we match our regular expression, format the line with a tag
		if matches := re.FindStringSubmatch(line); matches != nil {
			line = fmt.Sprintf("[USR] %s %s", matches[1], line)
		}
		// otherwise append the line as-is
		taggedLines = append(taggedLines, line)
	}

	return taggedLines
}
