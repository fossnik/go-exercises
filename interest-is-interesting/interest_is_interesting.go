package interest

// InterestRate returns the interest rate for the provided balance.
func InterestRate(balance float64) float32 {
	// 3.213% for a balance less than 0 dollars (balance gets more negative).
	if balance < 0 {
		return 3.213
	}

	// 0.5% for a balance greater than or equal to 0 dollars, and less than 1000 dollars.
	if balance < 1000 {
		return .5
	}

	// 1.621% for a balance greater than or equal to 1000 dollars, and less than 5000 dollars.
	if balance < 5000 {
		return 1.621
	}

	// 2.475% for a balance greater than or equal to 5000 dollars.
	return 2.475
}

// Interest calculates the interest for the provided balance.
func Interest(balance float64) float64 {
	return float64(InterestRate(balance)) * 0.01 * balance
}

// AnnualBalanceUpdate calculates the annual balance update, taking into account the interest rate.
func AnnualBalanceUpdate(balance float64) float64 {
	return balance + Interest(balance)
}

// YearsBeforeDesiredBalance calculates the minimum number of years required to reach the desired balance.
func YearsBeforeDesiredBalance(balance, targetBalance float64) int {
	years := 0

	for newBalance := balance; newBalance < targetBalance; years++ {
		newBalance = AnnualBalanceUpdate(newBalance)
	}

	return years
}
