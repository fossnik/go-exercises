// Package census simulates a system used to collect census data.
package census

// Resident represents a resident in this city.
type Resident struct {
	Name    string
	Age     int
	Address map[string]string
}

// NewResident registers a new resident in this city.
func NewResident(name string, age int, address map[string]string) *Resident {
	// create a new resident struct
	var newResident = &Resident{
		Name:    name,
		Age:     age,
		Address: address,
	}

	//*Resident: This is a pointer type. It represents a pointer to a Resident struct. When you declare a variable of type *Resident, you are declaring a variable that can hold the memory address of a Resident struct. You use * to dereference the pointer and access the fields of the struct it points to.
	//&Resident: This is the address-of operator. It returns the memory address of a Resident struct. When you use & before a variable of type Resident, you are obtaining a pointer to that variable, which is of type *Resident.

	return newResident
}

// HasRequiredInfo determines if a given resident has all of the required information.
func (r *Resident) HasRequiredInfo() bool {
	// return false if name less than 1 char
	if len(r.Name) < 1 {
		return false
	}

	// return false if street is blank
	if street := r.Address["street"]; len(street) < 1 {
		return false
	}

	// no required info is absent - return true
	return true
}

// Delete deletes a resident's information.
func (r *Resident) Delete() {
	// the pointer to r now points to a fresh new Resident that's blank
	*r = Resident{}
}

// Count counts all residents that have provided the required information.
func Count(residents []*Resident) int {
	// keep track of the count
	count := 0

	// loop through all residents
	for _, resident := range residents {
		if resident.HasRequiredInfo() {
			count++
		}
	}

	return count
}
