package booking

import "time"

// Schedule returns a time.Time from a string containing a date.
func Schedule(date string) time.Time {
	// parse the date
	t, err := time.Parse("1/2/2006 15:04:05", date)

	// return date if there was no error
	if err == nil {
		return t
	}

	// parse the date (format 2)
	t, err = time.Parse("January 2, 2006 15:04:05", date)

	// return date if there was no error
	if err == nil {
		return t
	}

	// parse the date (format 3)
	t, err = time.Parse("Monday, January 2, 2006 15:04:05", date)

	// return date if there was no error
	if err == nil {
		return t
	}

	// alert that the date couldn't be parsed
	panic("Unknown date format: " + date)
}

// HasPassed returns whether a date has passed.
func HasPassed(date string) bool {
	// get the time
	t := Schedule(date)

	// return true if it's from before now
	return t.Before(time.Now())
}

// IsAfternoonAppointment returns whether a time is in the afternoon.
func IsAfternoonAppointment(date string) bool {
	// get the time
	t := Schedule(date)

	// Get the hour component of the time
	hour := t.Hour()

	// Check if the hour is between 12:00 and 18:00
	return hour >= 12 && hour < 18
}

// Description returns a formatted string of the appointment time.
func Description(date string) string {
	// get the time
	t := Schedule(date)

	// Get a formatted string of the date
	formattedDate := t.Format("Monday, January 2, 2006")

	// Get a formatted string of the time
	formattedTime := t.Format("15:04")

	// create a natural language announcement of the appointment date and time
	outputString := "You have an appointment on " + formattedDate + ", at " + formattedTime + "."

	return outputString
}

// AnniversaryDate returns a Time with this year's anniversary.
func AnniversaryDate() time.Time {
	// get this year
	currentYear := time.Now().Year()

	// return the anniversary date during the current year
	return time.Date(currentYear, time.September, 15, 0, 0, 0, 0, time.UTC)
}
