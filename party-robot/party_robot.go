package partyrobot

import "fmt"

// Welcome greets a person by name.
func Welcome(name string) string {
	// return the greeting with the name of the person appended to it
	return fmt.Sprintf("Welcome to my party, %s!", name)
}

// HappyBirthday wishes happy birthday to the birthday person and exclaims their age.
func HappyBirthday(name string, age int) string {
	// append the string and integer to the happy birthday string
	return fmt.Sprintf("Happy birthday %s! You are now %d years old!", name, age)
}

// AssignTable assigns a table to each guest.
func AssignTable(name string, table int, neighbor, direction string, distance float64) string {
	// create a string
	outputString := ""

	// append welcome to my party
	outputString += Welcome(name)

	// append new line
	outputString += "\n"

	// append the table number
	outputString += fmt.Sprintf("You have been assigned to table %03d. Your table is %s, exactly %.1f meters from here.", table, direction, distance)

	// append new line
	outputString += "\n"

	// append table mate
	outputString += fmt.Sprintf("You will be sitting next to %s.", neighbor)

	return outputString
}
